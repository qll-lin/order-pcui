/*
 * @Description:
 * @Author: charles
 * @Date: 2023-10-15 15:25:06
 * @LastEditors: charles
 * @LastEditTime: 2023-10-15 15:25:07
 */
export default {
  data(){
    return {
      baseURL:process.env.VUE_APP_BASE_API,
      fileUrl:process.env.VUE_APP_FILE_URL,
    }
  }
}
