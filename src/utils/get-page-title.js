// import defaultSettings from '@/settings'

// const title = defaultSettings.title || 'Vue Element Admin'

export default function getPageTitle (pageTitle, projectName) {
  // console.log(pageTitle, projectName)
  if (pageTitle) {
    return `${pageTitle} - ${projectName}`
  }
  return `${projectName}`
}
