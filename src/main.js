/*
 * @Description:
 * @Author: charles
 * @Date: 2022-02-24 13:21:30
 * @LastEditors: charles
 * @LastEditTime: 2023-10-15 15:33:29
 */
import Vue from 'vue'

import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import Element from 'element-ui'
import './styles/element-variables.scss'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import './icons' // icon
import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters

import Vant from 'vant'
import 'vant/lib/index.css'

import '@/styles/university.css'

import JackyOrder from '@/components/estore/JackyOrder'
import urlMixin from '@/utils/urlMixin'


Vue.component('jacky-order', JackyOrder)

Vue.use(Vant)

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */
// import { mockXHR } from '../mock'
// if (process.env.NODE_ENV === 'production') {
//   mockXHR()
// }

Vue.use(Element, {
  size: Cookies.get('size') || 'medium' // set element-ui default size
})

// register global utility filters
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

Vue.mixin({
  ...urlMixin
});

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
})
