import { get, post } from '@/utils/request'
export default {
  namespaced: true,
  state: {
    baseConfig: []
  },
  mutations: {
    refreshBaseConfig (state, baseConfig) {
      state.baseConfig = baseConfig
    }
  },
  actions: {
    async findBaseConfig ({ commit }) {
      let response = await get("/baseConfig/findAll")
      commit('refreshBaseConfig', response.data)
      return response;
    }
  }
}
