import Cookies from 'js-cookie'
import { get } from '@/utils/request'
const state = {
  sidebar: {
    opened: Cookies.get('sidebarStatus') ? !!+Cookies.get('sidebarStatus') : true,
    withoutAnimation: false
  },
  dynamicRoutes: [],
  device: 'desktop',
  size: Cookies.get('size') || 'medium',
  logo: null,
  projectName: null,
  address: null,
  copyright: null,
}

const mutations = {
  // 设置全局logo
  SET_LOGO: (state, logo) => {
    state.logo = logo
  },
  // 设置全局项目名称
  SET_PROJECTNAME: (state, projectName) => {
    state.projectName = projectName
  },
  // 设置全局项目地址
  SET_ADDRESS: (state, address) => {
    state.address = address
  },
  // 设置全局项目版本备案号
  SET_COPYRIGHT: (state, copyright) => {
    state.copyright = copyright
  },
  RESET_DYNAMIC_ROUTES: (state, routes) => {
    state.dynamicRoutes = routes
  },
  TOGGLE_SIDEBAR: state => {
    state.sidebar.opened = !state.sidebar.opened
    state.sidebar.withoutAnimation = false
    if (state.sidebar.opened) {
      Cookies.set('sidebarStatus', 1)
    } else {
      Cookies.set('sidebarStatus', 0)
    }
  },
  CLOSE_SIDEBAR: (state, withoutAnimation) => {
    Cookies.set('sidebarStatus', 0)
    state.sidebar.opened = false
    state.sidebar.withoutAnimation = withoutAnimation
  },
  TOGGLE_DEVICE: (state, device) => {
    state.device = device
  },
  SET_SIZE: (state, size) => {
    state.size = size
    Cookies.set('size', size)
  }
}

const actions = {
  async findLogo ({ commit }) {
    let res = await get('/baseConfig/findByKey?name=logo')
    commit('SET_LOGO', res.data.value)
    return res.data.value
  },
  async findProjectName ({ commit }) {
    let res = await get('/baseConfig/findByKey?name=projectName')
    commit('SET_PROJECTNAME', res.data.value)
    return res.data.value
  },
  async findAddress ({ commit }) {
    let res = await get('/baseConfig/findByKey?name=address')
    commit('SET_ADDRESS', res.data.value)
    return res.data.value
  },
  async findCopyRight ({ commit }) {
    let res = await get('/baseConfig/findByKey?name=copyright')
    commit('SET_COPYRIGHT', res.data.value)
    return res.data.value
  },
  toggleSideBar ({ commit }) {
    commit('TOGGLE_SIDEBAR')
  },
  closeSideBar ({ commit }, { withoutAnimation }) {
    commit('CLOSE_SIDEBAR', withoutAnimation)
  },
  toggleDevice ({ commit }, device) {
    commit('TOGGLE_DEVICE', device)
  },
  setSize ({ commit }, size) {
    commit('SET_SIZE', size)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
